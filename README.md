## template projects

Versions: 

> * Symfony : 4.4
> * PHP : 7.4


## First run

Clone project

```bash
git clone git@gitlab.com:sidi.lekhalifa/template-projects.git
```

Launch all the containers and install all the dependencies with 


```bash
make build
```

Start project 

```bash
make up
```
Stop project 

```bash
make stop
```
