#!/bin/bash

echo "DOCKER INSTALLATION PROCESSES"
$(lsb_release -cs)
apt-get update -yqq
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -yqq
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -yqq
apt-get install docker-ce -yqq
echo "DOCKER INSTALLED"