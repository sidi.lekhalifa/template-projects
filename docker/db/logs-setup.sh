#!/bin/bash

ln -sf /dev/stdout /var/log/mysql/mysql.err
ln -sf /dev/stdout /var/log/mysql/mysql_slow_query.log
ln -sf /dev/stdout /var/log/mysql/mysql-general.log

chown mysql:mysql /var/log/mysql/mysql.err
chown mysql:mysql /var/log/mysql/mysql_slow_query.log
chown mysql:mysql /var/log/mysql/mysql-general.log

set -- /docker-entrypoint.sh "$@"
exec "$@"
