#!/bin/bash

# Generate permanent logs
mkdir -p /var/log/mysql

chown mysql.adm /var/log/mysql

touch /var/log/mysql/mysql.err
touch /var/log/mysql/mysql_slow_query.log

chown mysql.adm /var/log/mysql/mysql.err
chown mysql.adm /var/log/mysql/mysql_slow_query.log

# Setup mail on failure
sed -i "s/GET_MAIL_FROM_ENV/${NOTIF_EMAIL}/g" service-check
chmod +x /service-check

service cron start

# Configure exim4
sed -i "s/^\(dc_eximconfig_configtype=\)'local'/\1'internet'/g" /etc/exim4/update-exim4.conf.conf
sed -i "s/^\(dc_other_hostnames=\).*$/\1'pipedrive.carizy.com'/g" /etc/exim4/update-exim4.conf.conf
echo "pipedrive.carizy.com" > /etc/mailname
update-exim4.conf
service exim4 restart

# Return to mariadb entrypoint
set -- /docker-entrypoint.sh "$@"
exec "$@"
