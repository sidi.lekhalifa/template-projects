ENV=dev

include .env
export

CICD    = ${cicd}

ifeq ($(CICD), true)
FIG=
RUN=
EXEC=
VERSION=
endif



FIG?=docker-compose -f docker-compose.yml
ifeq ($(INFRA_ENV), dev)
    FIG   += -f docker-compose.dev.yml
endif
ifeq ($(INFRA_ENV), preprod)
    FIG   += -f docker-compose.preprod.yml
endif
ifeq ($(INFRA_ENV), ci)
    FIG   += -f docker-compose.ci.yml
endif
ifeq ($(INFRA_ENV), prod)
    FIG   += -f docker-compose.prod.yml
endif

RUN?=$(FIG) run --rm app
EXEC?=$(FIG) exec app
CONSOLE=php bin/console
PHPCSFIXER?=$(RUN) php -d memory_limit=1024m vendor/bin/php-cs-fixer
PHPMETRICS?=$(RUN) php -d memory_limit=1024m vendor/bin/phpmetrics
SAMI?=$(RUN) php bin/sami

.DEFAULT_GOAL := help
.PHONY: help start stop reset db db-diff db-migrate db-rollback db-load watch clear clean test tu tf tj lint ls ly lt
.PHONY: lj build up perm deps cc phpcs phpcsfix tty


help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

##
## Project setup
##---------------------------------------------------------------------------

start:            ## Install and start the project
start: build up db perm

down: docker-compose.yml ## Stop container
	$(FIG) stop

up: docker-compose.yml ## Start the containers
	$(FIG) up -d


stop:             ## Remove docker containers
stop:
	$(FIG) kill
	$(FIG) rm -v --force

down:             ## Stop docker containers
down:
	$(FIG) stop

reset:            ## Reset the whole project
reset: stop start

clear:            ## Remove all the cache, the logs, the sessions and the built assets
clear: perm
	-$(EXEC) rm -rf var/cache/*
	-$(EXEC) rm -rf var/sessions/*
	rm -rf var/logs/*
	#rm var/.php_cs.cache

clean:            ## Clear and remove dependencies
clean: clear
	rm -rf vendor node_modules

cc:               ## Clear the cache in dev env
cc:
	$(RUN) $(CONSOLE) cache:clear --no-warmup
	$(RUN) $(CONSOLE) cache:warmup

tty:              ## Run app container in interactive mode
tty:
	$(RUN) /bin/bash

##
## Database
##---------------------------------------------------------------------------

db:             ## Reset the database and run migrations
db: installvendor
	$(RUN) $(CONSOLE) doctrine:database:create --if-not-exists
	$(RUN) $(CONSOLE) doctrine:migrations:migrate -n

db-diff:        ## Generate a migration by comparing your current database to your mapping information
db-diff: installvendor
	$(RUN) $(CONSOLE) doctrine:migration:diff

db-migrate:     ## Migrate database schema to the latest available version
db-migrate: installvendor
	$(RUN) $(CONSOLE) doctrine:migration:migrate -n

db-rollback:    ## Rollback the latest executed migration
db-rollback: installvendor
	$(RUN) $(CONSOLE) d:m:e --down $(shell $(RUN) $(CONSOLE) d:m:l) -n

db-load:        ## Reset the database fixtures
db-load: installvendor
	$(RUN) $(CONSOLE) doctrine:fixtures:load -n

db-validate:    ## Check the ORM mapping
db-validate: installvendor
	$(RUN) $(CONSOLE) doctrine:schema:validate


# Internal rules

build:
	$(FIG) pull --parallel
	$(FIG) build --force-rm

up:
	$(FIG) up -d --remove-orphans

perm:
	-$(EXEC) chmod -R 777 var

# Rules from files

installvendor:
	$(RUN) composer install

updatevendor:
	$(RUN) composer install

vendor: composer.lock
	@$(RUN) composer install

composer.lock: composer.json
	@echo compose.lock is not up to date.

##
## Sentry
##---------------------------------------------------------------------------
sentry-deploy:      ## Deploy new release of sentry
sentry-deploy:
	$(RUN) sh sentry_deploy.sh

##
## Test
##---------------------------------------------------------------------------

test: # Run test
	$(RUN) ./vendor/bin/phpunit ./tests